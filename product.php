<?php 
if (isset($_GET["s"])) {
	$name = $_GET["s"];
	$product=query("SELECT * FROM items WHERE item_name LIKE %$name% ");
} elseif (isset($_GET["c"])) {
	$id = $_GET["c"];
	$product=query("SELECT * FROM items WHERE items.category_id = $id");
}else{
	$product=query("SELECT * FROM items");
}
?>

<?php foreach($product as $row): ?>
	<div class="col-lg-3 col-md-6 mb-4">
		<div class="card h-100 flat">
			<a href="product_detail.php?id=<?php echo $row['item_id'] ?>"><img class="card-img-top" width="100" height="130" src="uploads/<?php echo $row['item_image'] ?>" alt=""></a>
			<div class="card-body">
				<h5 class="card-title">
					<a href="product_detail.php?id=<?php echo $row['item_id'] ?>" class="text-kembang"><?php echo $row['item_name'] ?></a>
				</h5>
				<h6><?php echo 'Rp. '. number_format($row['item_price']) ?></h6>
			</div>
			<div class="card-footer">
				<a href="product_detail.php?id=<?php echo $row['item_id'] ?>" class="btn btn-success btn-sm btn-block">Detail</a>
			</div>
		</div>
	</div>
	<?php endforeach; ?>