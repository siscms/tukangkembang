<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tukang Kembang - Home Page</title>

  <link rel="shortcut icon" href="assets/img/favicon.png">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/custom_frontend.css" rel="stylesheet">
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>

<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg fixed-top navtk">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="ml-5 d-none d-md-inline-block form-inline mr-0 mr-md-3 my-2 my-md-0">
      <form action="index.php?s=bunga" class="" id="navBarSearchForm" method="POST">
        <div class="input-group">
          <input type="text" class="form-control flat" placeholder="Pencarian..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-search" type="submit">
              Cari
            </button>
          </div>
        </div>
      </form>
    </div>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active pr-3">
          <a class="nav-link white" href="index.php">Home
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link white" href="team.php">Team</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link white" href="about.php">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link btn btn-log btn-sm" href="admin/index.php">
            <?php if(!$_SESSION['login']) { ?>
            <i class="fa fa-user"></i> Login</a>
          <?php } else { ?>
            <i class="fa fa-user"></i> My Account</a>
          <?php } ?>
        </li>
      </ul>
    </div>
  </div>
</nav>