<?php include "header.php"; ?>

<style type="text/css">

.card-profile {
	width: 340px;
	margin: 50px auto;
	background-color: #fff;
	border-radius: 0;
	border: 0;
	box-shadow: 1em 1em 2em rgba(0, 0, 0, 0.2);
}
.card-profile .card-img-top {
	border-radius: 0;
}
.card-profile .card-img-profile {
	max-width: 100%;
	border-radius: 50%;
	margin-top: -95px;
	margin-bottom: 35px;
	border: 5px solid #fff;
}
.card-profile .card-title {
	margin-bottom: 50px;
}
.card-profile .card-title small {
	display: block;
	font-size: .6em;
	margin-top: .2em;
}
.card-profile .card-links {
	margin-bottom: 25px;
}
.card-profile .card-links .fab {
	margin: 0 1em;
	font-size: 1.6em;
}
.card-profile .card-links .fab:focus, .card-profile .card-links .fab:hover {
	text-decoration: none;
}
.card-profile .card-links .fab.fa-whatsapp {
  color: #26C281;
  font-weight: bold;
}
.card-profile .card-links .fab.fa-whatsapp:hover {
  color: #26C281;
}
.card-profile .card-links .fab.fa-twitter {
  color: #68aade;
}
.card-profile .card-links .fab.fa-twitter:hover {
  color: #3e92d5;
}
.card-profile .card-links .fab.fa-facebook {
  color: #3b5999;
}
.card-profile .card-links .fab.fa-facebook:hover {
  color: #2d4474;
}

</style>

<div class="container mt-5">
		<h4 class="text-center">Team Tukang Kembang</h4>
<div class="row">
	<div class="col-md-6">
		<div class="card card-profile text-center">
			<img alt="" class="card-img-top" src="assets/img/back.jpg">
			<div class="card-block">
				<img alt="" class="card-img-profile" src="assets/img/dev_1.jpg" height="200">
				<h4 class="card-title">
					Achyar Anshorie
					<small>Web Developer</small>
				</h4>
				<div class='card-links'>
					<a class='fab fa-whatsapp' href='http://api.whatsapp.com/send?phone=62816299081'></a>
					<a class='fab fa-twitter' href='https://twitter.com/achyarca'></a>
					<a class='fab fa-facebook' href='https://facebook.com/achyar.anshorie'></a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-profile text-center">
			<img alt="" class="card-img-top" src="assets/img/back.jpg">
			<div class="card-block">
				<img alt="" class="card-img-profile" src="assets/img/dev_2.jpg" height="200">
				<h4 class="card-title">
					Sistiandy Syahbana Nugraha
					<small>Web Developer</small>
				</h4>
				<div class='card-links'>
					<a class='fab fa-whatsapp' href='http://api.whatsapp.com/send?phone=628568125131'></a>
					<a class='fab fa-twitter' href='#'></a>
					<a class='fab fa-facebook' href='#'></a>
				</div>
			</div>
		</div>
	</div>

</div>
</div>



<?php include "footer.php"; ?>