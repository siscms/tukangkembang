-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 22, 2018 at 12:09 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tukangkembang`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Tanaman Hias Bunga'),
(2, 'Tanaman Hias Daun'),
(3, 'Tanaman Hias Buah'),
(4, 'Tanaman Hias Akar');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_price` decimal(10,0) DEFAULT NULL,
  `item_image` varchar(255) DEFAULT NULL,
  `item_desc` text,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `item_input_date` timestamp NULL DEFAULT NULL,
  `item_last_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_price`, `item_image`, `item_desc`, `category_id`, `user_id`, `item_input_date`, `item_last_update`) VALUES
(1, 'Bunga Melati', '50000', '5b7c276a5d547.jpg', 'Melati digolongkan sebagai tumbuhan perdu yang termasuk ke dalam macam-macam tanaman hias bunga. Tanaman merambat yang terdiri dari sekitar 200 spesies ini tumbuh dengan subur di daerah beriklim tropis dan hangat seperti Eurasia, Australasia, Oseania, dan Indonesia. Melati terkenal akan aroma wangi yang khas.', 1, 1, '2018-08-21 02:53:30', '2018-08-21 02:53:30'),
(2, 'Bunga Mawar', '70000', '5b7c27f92ccc7.jpg', 'Bunga mawar diberi julukan sebagai “Ratu Bunga” dikarenakan mempunyai keindahan dan keunikan yang khas. Bentuk yang indah pada kelopak bunga mawar didukung dengan aromanya yang menarik. Selain anggrek, tanaman hias bunga mawar juga diindentikan dengan simbol perasaan cinta terhadap seseorang.', 1, 1, '2018-08-21 02:55:53', '2018-08-21 02:55:53'),
(3, 'Suplir', '100000', '5b7c28586a93d.jpg', 'Selain suplir, tanaman ini juga dikenal sebagai Adiantum. Tanaman hias suplir atau adiantum mempunyai penampilan yang khas sehingga membuatnya mudah dibedakan dengan jenis tumbuhan paku-pakuan lain. Suplir yang termasuk ke dalam anak suku Vittarioideae, suku Pteridaceae ini banyak ditanam untuk menghias ruang atau taman.', 2, 1, '2018-08-21 02:57:28', '2018-08-21 02:57:28'),
(4, 'Palem Kuning', '90000', '5b7c28bc6c4c1.jpg', 'Palem kuning adalah tanaman hias yang tahan panas matahari yang tergabung dalam anggota suku pinang-pinangan, berasal dari Madagaskar dan sekarang bisa dijumpai dengan mudah di tanah air.', 2, 1, '2018-08-21 02:59:08', '2018-08-21 02:59:08'),
(5, 'Jeruk Kalamansi', '200000', '5b7c2ac466548.jpg', 'Jeruk Kalamansi disebut juga jeruk kasturi atau limau kasturi, dalam Bahasa Inggris disebut sebagai calamondin, calamansi, atau miniature citrus, dalam Bahasa Indonesia selain dipanggil jeruk kalamansi atau kasturi tersebut juga disebut jeruk kalamondin, di Bali dan juga dalam Bahasa Melayu disebut sebagai limau kesturi. Disebut miniature citrus atau jeruk mini karena banyak ditanam di pot untuk jadi tanaman hiasan atau keperluan ornamental', 3, 1, '2018-08-21 03:03:14', '2018-08-21 03:07:48'),
(6, 'Tanaman Stroberi', '250000', '5b7c2aba7b524.jpg', 'trawberry atau dalam ejaan Indonesia ditulis sesuai dengan cara bacanya, yaitu stroberi. Siapa yang tidak tau dengan buah ini? Buah strawberry mempunyai aroma dan rasa yang khas, jadi tidak heran jika buah ini sering digunakan sebagai perasa tambahan dalam makanan dan minuman. Rasa buah strawberry adalah antara asam dan manis (terkadang rasa asamnya jauh lebih menyengat ketimbang manisnya), jadi memang untung-untungan untuk mendapatkan rasa strawberry yang benar-benar manis.', 3, 1, '2018-08-21 03:05:53', '2018-08-21 03:07:38'),
(7, 'Tanaman Wahong', '300000', '5b7c2d1344342.jpg', 'Tanaman hias akar mempunyai keunggulan pada bagian akarnya. Beberapa jenis tanaman hias akar yang biasa ditanam adalah Ficus benyamina, Ficus elastica, Canarium spp., Artacarpus altilis, dan beberapa nama lain dari kelompok ataupun famili berbeda.', 4, 1, '2018-08-21 03:17:39', '2018-08-21 03:17:39'),
(8, 'Tanaman Jambu', '60000', '5b7c2de2b10a8.jpg', 'Jambu air adalah tumbuhan dalam suku jambu-jambuan atau Myrtaceae yang berasal dari Asia Tenggara. Jambu air sebetulnya berbeda dengan JEMBUT BATU, kerabat dekatnya yang memiliki pohon dan buah hampir serupa', 3, 1, '2018-08-21 03:21:06', '2018-08-21 03:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(45) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_full_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_username`, `user_password`, `user_full_name`) VALUES
(1, 'admin', '$2y$10$vLR9a9UYIkrmsjQ5H3tThuPmn.Eqm0FZxQeVKBCODmiFI8IzLUvSu', 'Achyar Anshorie'),
(2, 'sis', '$2y$10$WFQPaNfhyYKFrAztSWvB9uY.H8F.5VcvcSM/Q6ATV8iiFNg7c2lcu', 'Sistiandy Syahbana Nugraha');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `fk_items_categories_idx` (`category_id`),
  ADD KEY `fk_items_users1_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_items_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_items_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
