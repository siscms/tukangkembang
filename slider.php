<div id="carouselExampleIndicators" class="carousel slide my-4 mt-5" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner" role="listbox">
		<div class="carousel-item active">
			<img class="d-block img-fluid" src="assets/img/slider/banner_1.jpg" alt="First slide">
		</div>
		<div class="carousel-item">
			<img class="d-block img-fluid" src="assets/img/slider/banner_2.jpg" alt="Second slide">
		</div>
		<div class="carousel-item">
			<img class="d-block img-fluid" src="assets/img/slider/banner_3.jpg" alt="Third slide">
		</div>
	</div>
</div>