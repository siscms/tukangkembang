<?php 
require 'functions.php';


$id = $_GET["id"];

$category= query("SELECT * FROM categories WHERE category_id = $id")[0];

if(isset($_POST["edit"]))
{
	if(edit_category($_POST)>0)
	{
		echo "<script>alert('kategori berhasil diubah');
		document.location.href='category.php';
		</script>";
	}
	else
	{
		echo "<script>alert('kategori gagal diubah');
			document.location.href='category.php';
			</script>
			";
	}
}
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Edit Pengguna
			</div>
			<div class="card-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-9">
							<input type="hidden" name="category_id" value="<?php echo $category['category_id']; ?>">
							
							<div class="form-group">
								<label>Nama Kategori *</label>
								<input type="text" class="form-control" name="category_name" placeholder="isi nama lengkap" value="<?php echo $category['category_name']; ?>">
							</div>

							<small>*) Wajib diisi</small>

						</div>
						<div class="col-md-3 mt-4">
							<button type="submit" name="edit" class="btn btn-success btn-block">Simpan</button>
							<a href="category.php" class="btn btn-danger btn-block">Batal</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

	</div>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->