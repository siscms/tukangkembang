<?php 
require "functions.php";

if(isset($_POST["add"]))
{
	if(add_user($_POST)>0)
	{
		echo "<script>alert('user berhasil ditambahkan');
		document.location.href='users.php';
		</script>";
	}
	else
	{
		echo mysqli_error($con);
	}
}
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Tambah Pengguna
			</div>
			<div class="card-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-9">
							
							<div class="form-group">
								<label>Nama Lengkap *</label>
								<input type="text" class="form-control" name="user_full_name" placeholder="isi nama lengkap">
							</div>

							<div class="form-group">
								<label>Username *</label>
								<input type="text" class="form-control" name="user_username" placeholder="isi username">
							</div>

							<div class="form-group">
								<label>Password *</label>
								<input type="password" class="form-control" name="user_password" placeholder="isi password">
							</div>

							<div class="form-group">
								<label>Konfirmasi Password *</label>
								<input type="password" class="form-control" name="confirm" placeholder="konfirmasi password">
							</div>

							<small>*) Wajib diisi</small>

						</div>
						<div class="col-md-3 mt-4">
							<button type="submit" name="add" class="btn btn-success btn-block">Simpan</button>
							<a href="users.php" class="btn btn-danger btn-block">Batal</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

	</div>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->