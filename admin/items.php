<?php 
session_start();
if(!isset($_SESSION["login"]))
{
	header("location:login.php");
	exit;
}
require 'functions.php';

$item = query("SELECT items.item_id, items.item_name, items.item_price, items.item_image, items.item_desc, items.item_input_date, items.item_last_update, items.category_id, items.user_id, users.user_full_name, categories.category_name FROM items 
	LEFT JOIN categories ON items.category_id = categories.category_id
	LEFT JOIN users ON items.user_id = users.user_id 
	ORDER BY items.item_input_date DESC
	");

	?>

	<!-- Header -->
	<?php include '../admin/header.php'; ?>
	<!-- Close Header -->

	<!-- Sidebar -->
	<?php include '../admin/sidebar.php'; ?>
	<!-- Close Sidebar -->
	<div id="content-wrapper">
		<div class="container-fluid">

			<div class="card mb-3">
				<div class="card-header">
					Daftar Produk
					<a href="items_add.php" class="btn btn-success btn-sm float-right"><i class="fa fa-plus"></i> Tambah</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead class="thead-dark">
								<tr>
									<th>No</th>
									<th>Nama Produk</th>
									<th>Kategori Produk</th>
									<th>Harga Produk</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i=1;
								foreach($item as $row) :
									?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $row['item_name'] ?></td>
										<td><?php echo $row['category_name'] ?></td>
										<td><?php echo 'Rp. ' . number_format($row['item_price']) ?></td>
										<td>
											<a href="items_edit.php?id=<?php echo $row['item_id']; ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
											<a href="items_view.php?id=<?php echo $row['item_id']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
											<a href="items_delete.php?id=<?=$row['item_id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah akan menghapus data ini ?')"><i class="fa fa-trash"></i></a>
											<td>
											</tr>
											<?php 
											$i++;
										endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>

				<!-- Footer -->
				<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->