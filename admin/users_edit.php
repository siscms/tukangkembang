<?php 
require 'functions.php';


$user_id = $_GET["user_id"];

$user= query("SELECT * FROM users WHERE user_id=$user_id")[0];

if(isset($_POST["edit"]))
{
	if(edit_user($_POST)>0)
	{
		echo "<script>alert('user berhasil diubah');
		document.location.href='users.php';
		</script>";
	}
	else
	{
		echo "<script>alert('user gagal diubah');
			document.location.href='users.php';
			</script>
			";
	}
}
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Edit Pengguna
			</div>
			<div class="card-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-9">
							<input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>">
							<input type="hidden" name="user_password" value="<?php echo $user['user_password']; ?>">
							<div class="form-group">
								<label>Username *</label>
								<input type="text" class="form-control" name="user_username" placeholder="isi username" value="<?php echo $user['user_username']; ?>" readonly >
							</div>

							<div class="form-group">
								<label>Nama Lengkap *</label>
								<input type="text" class="form-control" name="user_full_name" placeholder="isi nama lengkap" value="<?php echo $user['user_full_name']; ?>">
							</div>

							<small>*) Wajib diisi</small>

						</div>
						<div class="col-md-3 mt-4">
							<button type="submit" name="edit" class="btn btn-success btn-block">Simpan</button>
							<a href="users.php" class="btn btn-danger btn-block">Batal</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

	</div>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->