<?php

$host = 'localhost';
$user_db = 'root';
$pwd_db = 'root';
$database = 'tukangkembang';
$con=mysqli_connect($host, $user_db, $pwd_db, $database);

function query($query) 
{
	global $con;

	$result=mysqli_query($con,$query);
	$rows=[];
	while($row=mysqli_fetch_assoc($result))
	{

		$rows[]=$row;
	}
	return $rows;
}


function register($data)
{
	global $con;
	$name = htmlspecialchars($data["user_full_name"]);
	$username = strtolower(stripslashes($data["user_username"]));
	$password = mysqli_real_escape_string($con,$data["user_password"]);

	$confirm = mysqli_real_escape_string($con,$data["confirm"]);

	if($password !== $confirm) 
	{
		echo "<script>alert('Konfirmasi password tdk sesuai');</script>";
		return false;
	}

	$result=mysqli_query($con,"SELECT user_username FROM users WHERE user_username='$username'");
	if(mysqli_fetch_assoc($result))
	{
		echo "<script> alert('Username sudah ada');
		</script>";
		return false;
	}

	$password = password_hash($password, PASSWORD_DEFAULT);

	mysqli_query($con,"INSERT INTO `users` VALUES('', '$username', '$password','$name')");

	return mysqli_affected_rows($con);
}

// Users

function add_user($data)
{

	global $con;	

	$name = htmlspecialchars($data["user_full_name"]);
	$username = strtolower(stripslashes($data["user_username"]));
	$password = mysqli_real_escape_string($con,$data["user_password"]);
	$confirm = mysqli_real_escape_string($con,$data["confirm"]);

	if($password !== $confirm) 
	{
		echo "<script>alert('Konfirmasi password tdk sesuai');</script>";
		return false;
	}

	$result=mysqli_query($con,"SELECT user_username FROM users WHERE user_username='$username'");
	if(mysqli_fetch_assoc($result))
	{
		echo "<script> alert('Username sudah ada');
		</script>";
		return false;
	}

	$password = password_hash($password, PASSWORD_DEFAULT);

	mysqli_query($con,"INSERT INTO `users` VALUES('', '$username', '$password','$name')");

	return mysqli_affected_rows($con);

}

function edit_user($data) {

	global $con;	

	$user_id = $data['user_id'];
	$name = htmlspecialchars($data["user_full_name"]);
	$username = strtolower(stripslashes($data["user_username"]));
	$password = $data['user_password'];


	$query = "UPDATE users SET

	user_username='$username',
	user_password='$password',
	user_full_name='$name'

	WHERE user_id=$user_id";

	mysqli_query($con,$query);
	return mysqli_affected_rows($con);

}

function delete_user($user_id) {
	global $con;
	mysqli_query($con,"DELETE FROM users WHERE user_id= $user_id");

	return mysqli_affected_rows($con);

}

// Close users

// Category

function add_category($data)
{

	global $con;

	$name = htmlspecialchars($data["category_name"]);

	mysqli_query($con,"INSERT INTO `categories` VALUES('', '$name')");

	return mysqli_affected_rows($con);

}

function edit_category($data) {

	global $con;	

	$id = $data['category_id'];
	$name = htmlspecialchars($data["category_name"]);

	$query = "UPDATE categories SET

	category_name='$name'

	WHERE category_id=$id";

	mysqli_query($con,$query);
	return mysqli_affected_rows($con);

}

function delete_category($id) {
	global $con;
	mysqli_query($con,"DELETE FROM categories WHERE category_id= $id");

	return mysqli_affected_rows($con);

}
// Close category

// Items

function add_item($data)
{

	global $con;

	$name = htmlspecialchars($data["item_name"]);
	$price = htmlspecialchars($data["item_price"]);
	$desc = htmlspecialchars($data["item_desc"]);
	$category = htmlspecialchars($data["category_id"]);
	$user = htmlspecialchars($data["user_id"]);
	$date_input = date('Y-m-d h:i:s');
	$date_update = date('Y-m-d h:i:s');

	//upload gambar
	$gambar = upload();
	if(!gambar)
	{
		return false;
	}

	mysqli_query($con,"INSERT INTO `items` VALUES(
		'', 
		'$name',
		'$price',
		'$gambar',
		'$desc',
		'$category',
		'$user',
		'$date_input',
		'$date_update'
	)");

	return mysqli_affected_rows($con);

}

function edit_item($data) {

	global $con;	

	$id = $data['item_id'];
	$name = htmlspecialchars($data["item_name"]);
	$price = htmlspecialchars($data["item_price"]);
	$desc = htmlspecialchars($data["item_desc"]);
	$category = htmlspecialchars($data["category_id"]);
	$user = htmlspecialchars($data["user_id"]);
	$date_input = $data["item_input_date"];
	$date_update = date('Y-m-d h:i:s');
	$gambarLama = htmlspecialchars($data["gambarLama"]);

	//cek apakah user pilih gambar baru/tdk
	if($_FILES['item_image']['error']===4) {
		$gambar = $gambarLama;
	} else {
		$gambar = upload();
	}

	$query = "UPDATE items SET

	item_name='$name',
	item_price='$price',
	item_image='$gambar',
	item_desc='$desc',
	category_id='$category',
	user_id='$user',
	item_input_date='$date_input',
	item_last_update='$date_update'

	WHERE item_id=$id";

	mysqli_query($con,$query);
	return mysqli_affected_rows($con);

}

function delete_item($id) {
	global $con;
	mysqli_query($con,"DELETE FROM items WHERE item_id= $id");

	return mysqli_affected_rows($con);

}

function upload()
{
	$namaFile = $_FILES['item_image']['name'];
	$ukuranFile = $_FILES['item_image']['size'];
	$error = $_FILES['item_image']['error'];
	$tmpName = $_FILES['item_image']['tmp_name'];

// cek apa tidak ada gambar yg di upload
	if($error === 4)
	{
		echo "<script>alert('pilih gambar dahulu');</script>";
		return false;
	}

	$ektensiGambarValid = ['jpg','png','jpeg'];
	$ekstensiGambar = explode('.',$namaFile);
	$ekstensiGambar = strtolower(end($ekstensiGambar));
	if(!in_array($ekstensiGambar,$ektensiGambarValid))
	{

		echo "<script>alert('yang di upload bukan gambar');</script>";
		return false;
	}

	if($ukuranFile > 1000000 )
	{
		echo "<script>alert('ukuran terlalu besar');</script>";
		return false;
	}
 		//generate nama gambar baru
	$namaFileBaru = uniqid();
	$namaFileBaru .= '.';
	$namaFileBaru .= $ekstensiGambar;
 		// lolos pengcekan
	move_uploaded_file($tmpName,'../uploads/' . $namaFileBaru);
	return $namaFileBaru;
}
// Close Items

?>