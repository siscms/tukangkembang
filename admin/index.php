<?php 
session_start();
if(!isset($_SESSION["login"]))
{
  header("location:login.php");
  exit;
}
?>
<?php 
require "functions.php";
$cat = query("SELECT category_id FROM categories");
$item = query("SELECT item_id FROM items");
$user = query("SELECT user_id FROM users");
?>
<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->

<!-- Main -->
<div id="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="card bg-info text-white mb-3">
          <div class="card-header">
            Kategori
          </div>
          <div class="card-body">
            Jumlah : <?php echo count($cat); ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card bg-success text-white mb-3">
          <div class="card-header">
            Produk
          </div>
          <div class="card-body">
            Jumlah : <?php echo count($item); ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card bg-warning text-white mb-3">
          <div class="card-header">
            Pengguna
          </div>
          <div class="card-body">
            Jumlah : <?php echo count($user); ?>
          </div>
        </div>
      </div>
    </div>


  </div>  
  <!-- Close Main -->

  <!-- Footer -->
  <?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->




