<?php 
session_start();
if(!isset($_SESSION["login"]))
{
	header("location:login.php");
	exit;
}
require 'functions.php';
$user=query("SELECT * FROM users");
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Daftar Pengguna
				<a href="users_add.php" class="btn btn-success btn-sm float-right"><i class="fa fa-plus"></i> Tambah</a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead class="thead-dark">
							<th>No</th>
							<th>Nama</th>
							<th>Username</th>
							<th>Password</th>
							<th>Aksi</th>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($user as $row) :
								?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $row['user_full_name'] ?></td>
									<td><?php echo $row['user_username'] ?></td>
									<td>**********</td>
									<td>
										<a href="users_edit.php?user_id=<?php echo $row['user_id']; ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
										<a href="users_delete.php?user_id=<?=$row['user_id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah akan menghapus data ini ?')"><i class="fa fa-trash"></i></a>
										<td>
								</tr>
								<?php 
								$i++;
							endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->