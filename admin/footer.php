<footer class="sticky-footer">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright © 2018 - <?php echo date('Y') ?></span>
		</div>
	</div>
</footer>

</div>
</div>


<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="../assets/js/custom.js"></script>

</body>
</html>