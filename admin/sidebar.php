<!-- Sidebar -->
<div id="wrapper">

<ul class="sidebar navbar-nav">
  <img src="../assets/img/person-flat.png" height="100" width="100" class="mx-auto mt-3 d-none d-sm-block">
  <p class="text-center d-none d-sm-block text-white"><?php echo $_SESSION['ufullname'] ?></p>
  <li class="nav-item active">
    <a class="nav-link" href="../index.php" target="_blank">
      <i class="fas fa-fw fa-globe"></i>
      <span>Lihat Web</span>
    </a>
  </li>
  <li class="nav-item active">
    <a class="nav-link" href="index.php">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <li class="nav-item dropdown active">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-fw fa-shopping-cart"></i>
      <span>Produk</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <a class="dropdown-item" href="category.php">Kategori</a>
      <a class="dropdown-item" href="items.php">Daftar Produk</a>
      
    </div>
  </li>
  <li class="nav-item dropdown active">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-fw fa-users"></i>
      <span>Pengguna</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <a class="dropdown-item" href="users.php">Daftar Pengguna</a>
      
    </div>
  </li>
</ul>