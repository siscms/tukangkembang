<?php
session_start();
if(isset($_SESSION["login"]))
{
  header("location:index.php");
  exit;
} 
require "functions.php";
if(isset($_POST["login"]))
{
  $username=$_POST["user_username"];
  $password=$_POST["user_password"];

  $result= mysqli_query($con,"SELECT * FROM users WHERE user_username = '$username'");

  //cek username
  if(mysqli_num_rows($result) === 1)
  {
    $row = mysqli_fetch_assoc($result);
    if(password_verify($password,$row["user_password"]) )
    {
      $_SESSION["login"]      = TRUE;
      $_SESSION["uid"]        = $row["user_id"];
      $_SESSION["uname"]      = $row["user_username"];
      $_SESSION["ufullname"]  = $row["user_full_name"];
      echo "<script>alert('Login Sukses');document.location.href='index.php'</script>";
      exit;

    }

  }
  $error = TRUE;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Tukang Kembang - Login</title>

  <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="../assets/css/custom.css" rel="stylesheet">

</head>

<body>

  <?php 
  if(isset($error)): ?>
    <div style='margin-bottom:-55px' class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-warning-sign'></span>  Login Gagal !! Username dan Password Salah !!</div>
    <?php  
  endif;
  ?>

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">
        <img src="../assets/img/logo.png" height="50" class="mx-auto">
      </div>
      <div class="card-body">
        <form action="" method="POST">

          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="user_username" id="inputUsername" class="form-control" placeholder="username" required="required" autofocus="autofocus">
              <label for="inputUsername">Username</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <input type="password" name="user_password" id="inputPassword" class="form-control" placeholder="Password" required="required">
              <label for="inputPassword">Password</label>
            </div>
          </div>
          <button class="btn btn-success btn-block" type="submit" name="login">Login</button>

        </form>
      </div>
    </div>
  </div>

</body>
</html>
