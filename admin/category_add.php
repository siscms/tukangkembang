<?php 
require "functions.php";

if(isset($_POST["add"]))
{
	if(add_category($_POST)>0)
	{
		echo "<script>alert('kategori berhasil ditambahkan');
		document.location.href='category.php';
		</script>";
	}
	else
	{
		echo mysqli_error($con);
	}
}
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Tambah Kategori
			</div>
			<div class="card-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-9">
							
							<div class="form-group">
								<label>Nama Kategori *</label>
								<input type="text" class="form-control" name="category_name" placeholder="isi nama kategori">
							</div>

							<small>*) Wajib diisi</small>

						</div>
						<div class="col-md-3 mt-4">
							<button type="submit" name="add" class="btn btn-success btn-block">Simpan</button>
							<a href="category.php" class="btn btn-danger btn-block">Batal</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

	</div>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->