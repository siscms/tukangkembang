<?php 
require "functions.php";
$cat = query("SELECT * FROM categories");

if(isset($_POST["add"]))
{ 
	if(add_item($_POST)>0)
	{
		echo "<script>alert('produk berhasil ditambahkan');
		document.location.href='items.php';
		</script>";
	}
	else
	{
		echo mysqli_error($con);
	}
}
?>

<!-- Header -->
<?php include '../admin/header.php'; ?>
<!-- Close Header -->

<!-- Sidebar -->
<?php include '../admin/sidebar.php'; ?>
<!-- Close Sidebar -->
<div id="content-wrapper">
	<div class="container-fluid">

		<div class="card mb-3">
			<div class="card-header">
				Tambah Produk
			</div>
			<div class="card-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-9">
							<input type="hidden" name="user_id" value="<?php echo $_SESSION['uid'] ?>">
							<div class="form-group">
								<label>Kategori *</label>
								<select name="category_id" class="form-control">
									<?php foreach ($cat as $row): ?>
										<option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>

							<div class="form-group">
								<label>Nama Produk *</label>
								<input type="text" class="form-control" name="item_name" placeholder="isi nama produk">
							</div>

							<div class="form-group">
								<label>Harga *</label>
								<input type="text" class="form-control" name="item_price" placeholder="isi harga">
							</div>

							<div class="form-group">
								<label>Deskripsi</label>
								<textarea class="form-control" name="item_desc" placeholder="isi deskripsi"></textarea>
							</div>

							<small>*) Wajib diisi</small>

						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Foto Produk</label>
								<img src="../assets/img/missing.png" id="target" alt="Choose image to upload" class="img-thumbnail">
								<input type="file" name="item_image" id="item_image" class="form-control">
							</div>
							<button type="submit" name="add" class="btn btn-success btn-block">Simpan</button>
							<a href="items.php" class="btn btn-danger btn-block">Batal</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

	</div>
	<script type="text/javascript">
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#target').attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#item_image").change(function() {
			readURL(this);
		});
	</script>

	<!-- Footer -->
	<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->