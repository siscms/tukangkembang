<?php 
session_start();
if(!isset($_SESSION["login"]))
{
	header("location:login.php");
	exit;
}
require 'functions.php';

$id = $_GET["id"];

$item = query("SELECT items.item_id, items.item_name, items.item_price, items.item_image, items.item_desc, items.item_input_date, items.item_last_update, items.category_id, items.user_id, users.user_full_name, categories.category_name FROM items 
	LEFT JOIN categories ON items.category_id = categories.category_id
	LEFT JOIN users ON items.user_id = users.user_id WHERE item_id = $id
	ORDER BY items.item_input_date DESC")[0];

	?>

	<!-- Header -->
	<?php include '../admin/header.php'; ?>
	<!-- Close Header -->

	<!-- Sidebar -->
	<?php include '../admin/sidebar.php'; ?>
	<!-- Close Sidebar -->
	<div id="content-wrapper">
		<div class="container-fluid">

			<div class="card mb-3">
				<div class="card-header">
					Detail Produk
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-3">
							<?php if (isset($item['item_image']) != NULL) { ?>
								<img src="../uploads/<?php echo $item['item_image']; ?>" class="img-thumbnail">
							<?php } else { ?>
								<img src="../assets/img/missing.png" class="img-thumbnail">
							<?php } ?>
						</div>
						<div class="col-md-9">
							<div class="table-responsive">
								<table class="table table-stripped">
									<tr>
										<td>Nama Produk</td>
										<td>:</td>
										<td><?php echo $item['item_name'] ?></td>
									</tr>
									<tr>
										<td>Kategori Produk</td>
										<td>:</td>
										<td><?php echo $item['category_name'] ?></td>
									</tr>
									<tr>
										<td>Harga Produk</td>
										<td>:</td>
										<td><?php echo 'Rp. '. number_format($item['item_price']) ?></td>
									</tr>
									<tr>
										<td>Deskripsi Produk</td>
										<td>:</td>
										<td><?php echo $item['item_desc'] ?></td>
									</tr>
									<tr>
										<td>Tanggal Buat</td>
										<td>:</td>
										<td><?php echo $item['item_input_date'] ?></td>
									</tr>
									<tr>
										<td>User Buat</td>
										<td>:</td>
										<td><?php echo $item['user_full_name'] ?></td>
									</tr>
								</table>
								<a href="items.php" class="btn btn-primary btn-sm">Kembali</a>
								<a href="items_edit.php?id.php?id=<?php echo $item['item_id']; ?>" class="btn btn-success btn-sm">Ubah</a>
								<a href="items_delete.php?id.php?id.php?id=<?php echo $item['item_id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah akan menghapus data ini ?')">Hapus</a>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>

		<!-- Footer -->
		<?php include "../admin/footer.php"; ?>  
  <!-- Close Footer -->