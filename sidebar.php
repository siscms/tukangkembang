<?php 
$category=query("SELECT * FROM categories");
?>

<div class="container-fluid">

	<div class="row">

		<div class="col-lg-3 mt-5">
			<div class="card flat">
				<div class="card-header">
					Kategori
				</div>
				<ul class="list-group list-group-flush">
					<?php foreach($category as $row): ?>
						<li class="list-group-item"><a href="index.php?c=<?php echo $row['category_id']  ?>" class="text-kembang"><?php echo $row['category_name'] ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>

		</div>
			<!-- /.col-lg-3 -->