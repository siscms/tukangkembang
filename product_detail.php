<?php require 'admin/functions.php'; ?>

<?php include 'header.php'; ?>

<?php 
$id = $_GET["id"];
$item = query("SELECT items.item_id, items.item_name, items.item_price, items.item_image, items.item_desc, items.item_input_date, items.item_last_update, items.category_id, items.user_id, users.user_full_name, categories.category_name FROM items 
	LEFT JOIN categories ON items.category_id = categories.category_id
	LEFT JOIN users ON items.user_id = users.user_id WHERE item_id = $id
	ORDER BY items.item_input_date DESC")[0];
	?>

	<div class="container mt-5 mb-5">
		<h4 class="mb-3">Detail Produk</h4>
		<div class="row">
			<div class="col-md-3">
				<img src="uploads/<?php echo $item['item_image'] ?>" class="img-thumbnail">
			</div>
			<div class="col-md-9">
				<div class="table-responsive">
					<table class="table table-hover">
						<tr>
							<td>Nama Produk</td>
							<td>:</td>
							<td><?php echo $item['item_name'] ?></td>
						</tr>
						<tr>
							<td>Kategori Produk</td>
							<td>:</td>
							<td><?php echo $item['category_name'] ?></td>
						</tr>
						<tr>
							<td>Harga Produk</td>
							<td>:</td>
							<td><?php echo 'Rp. '. number_format($item['item_price']) ?></td>
						</tr>
						<tr>
							<td>Deskripsi Produk</td>
							<td>:</td>
							<td><?php echo $item['item_desc'] ?></td>
						</tr>
						<tr>
							<td>Terakhir Update</td>
							<td>:</td>
							<td><?php echo $item['item_last_update'] ?></td>
						</tr>
					</table>
					<a href="index.php" class="btn btn-secondary"><i class="fa fa-arrow-left"></i> Kembali</a>
					<a href="http://api.whatsapp.com/send?phone=62816299081" class="btn btn-success"><i class="fab fa-whatsapp"></i> Pesan Sekarang</a>
				</div>
			</div>


		</div>
	</div>











<?php include 'footer.php'; ?>


